package com.example.bayuramadeza.tutorial_kampus.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bayuramadeza.tutorial_kampus.R;

/**
 * Created by bayuramadeza on 3/3/2018.
 */

public class ExploreFragment extends android.support.v4.app.Fragment {
    public ExploreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }
}
